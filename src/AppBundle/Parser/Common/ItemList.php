<?php

namespace AppBundle\Parser\Common;

/**
 * Class ItemList
 * @package AppBundle\Parser\Common;
 */
class ItemList implements \IteratorAggregate, \Countable
{
    /** @var array $itemsList */
    private $itemList = array();

    /** @var string $className */
    private $className;

    /**
     * @param string $className
     */
    public function __construct($className)
    {
        $this->className = $className;
    }

    /**
     * @param $item
     *
     * @return bool
     * @throws \Exception
     */
    public function add($item)
    {
        if ($item instanceof $this->className) {
            $this->itemList[] = $item;

            return true;
        }

        throw new \Exception("Item could not be added");
    }

    /**
     * @param mixed $item
     * @return mixed|null
     */
    public function remove($item)
    {
        if ($item instanceof $this->className) {
            foreach ($this->itemList as $key => $element) {
                if ($element == $item) {
                    $itemToRemove = $this->itemList[$key];
                    unset($this->itemList[$key]);

                    return $itemToRemove;
                }
            }

            return null;
        }

        return null;
    }

    /**
     * (PHP 5 >= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->itemList);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     * </p>
     * <p>
     * The return value is cast to an integer.
     */
    public function count()
    {
        return count($this->itemList);
    }

    public function chunk($size)
    {
        return array_chunk($this->itemList, $size);
    }
}
