<?php

namespace AppBundle\Parser\Common;

/**
 * Class AbstractDatabaseParser
 * @package AppBundle\Parser\Common;
 */
abstract class AbstractParser
{
    /** @var array $validKeys */
    protected $validKeys = array();

    /** @var ItemList $itemList */
    protected $itemList;

    /**
     * @param array $record
     *
     * @return mixed
     */
    abstract protected function parseIndividualRecordToObject(array $record);

    /**
     * @param ItemList $itemList
     */
    public function __construct(ItemList $itemList)
    {
        $this->itemList = $itemList;
    }

    /**
     * @param array $records
     *
     * @return ItemList
     * @throws \Exception
     */
    public function getObjectListFromRecords(array $records)
    {
        /** @var \AppBundle\Parser\Common\ItemList $objectList */
        $objectList = $this->itemList;

        foreach ($records as $record) {
            if ($this->hasValidKeys(array_keys($record))) {
                $objectList->add($this->parseIndividualRecordToObject($record));
            }
        }

        return $objectList;
    }

    /**
     * @param array $recordsKeys
     *
     * @return bool
     * @throws \Exception
     */
    protected function hasValidKeys(array $recordsKeys)
    {
        if (empty($this->validKeys)) {
            throw new \Exception('The variable $validKeys cannot be empty');
        }

        foreach ($recordsKeys as $key) {
            if (!in_array($key, $this->validKeys)) {
                throw new \Exception("Parameters '$key' for Class ".basename($this->itemList)
                ." couldn't be found in validKeys");
            }
        }

        return true;
    }
}
