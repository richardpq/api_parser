<?php

namespace AppBundle\Parser\Entity;

class ProductSaleElement extends ProductBase
{
    /** @var  boolean $default */
    protected $default;

    /** @var  int $productId */
    protected $productId;

    /**
     * @return boolean
     */
    public function isDefault()
    {
        return $this->default;
    }

    /**
     * @param boolean $default
     *
     * @return ProductSaleElement
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * @param int $productId
     *
     * @return ProductSaleElement
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }
}
