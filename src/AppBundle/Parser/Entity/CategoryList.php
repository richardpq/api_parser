<?php

namespace AppBundle\Parser\Entity;

use AppBundle\Parser\Common\ItemList;

class CategoryList extends ItemList
{
    const ENTITY_CLASS = Category::class;

    /**
     *
     */
    public function __construct()
    {
        parent::__construct(self::ENTITY_CLASS);
    }
}
