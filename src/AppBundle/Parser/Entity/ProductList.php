<?php

namespace AppBundle\Parser\Entity;

use AppBundle\Parser\Common\ItemList;

class ProductList extends ItemList
{
    const ENTITY_CLASS = Product::class;

    /**
     *
     */
    public function __construct()
    {
        parent::__construct(self::ENTITY_CLASS);
    }
}
