<?php

namespace AppBundle\Parser\Entity;

use AppBundle\Parser\Common\DescriptiveTrait;

class ProductImage
{
    use DescriptiveTrait;

    /** @var int $id */
    protected $id;

    /** @var string $locale */
    protected $locale;

    /** @var string $originalImagePath */
    protected $originalImagePath;

    /** @var string $title */
    protected $title;

    /** @var boolean $visible */
    protected $visible;

    /** @var int $position */
    protected $position;

    /** @var string $objectType */
    protected $objectType;

    /** @var int $objectId */
    protected $objectId;

    /** @var string $imageUrl */
    protected $imageUrl;

    /** @var string $originalImageUrl */
    protected $originalImageUrl;

    /** @var string $imagePath */
    protected $imagePath;

    /** @var boolean $processingError  */
    protected $processingError;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return ProductImage
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     *
     * @return ProductImage
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalImagePath()
    {
        return $this->originalImagePath;
    }

    /**
     * @param string $originalImagePath
     *
     * @return ProductImage
     */
    public function setOriginalImagePath($originalImagePath)
    {
        $this->originalImagePath = $originalImagePath;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return ProductImage
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isVisible()
    {
        return $this->visible;
    }

    /**
     * @param boolean $visible
     *
     * @return ProductImage
     */
    public function setVisible($visible)
    {
        $this->visible = (boolean) $visible;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return ProductImage
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string
     */
    public function getObjectType()
    {
        return $this->objectType;
    }

    /**
     * @param string $objectType
     *
     * @return ProductImage
     */
    public function setObjectType($objectType)
    {
        $this->objectType = $objectType;

        return $this;
    }

    /**
     * @return int
     */
    public function getObjectId()
    {
        return $this->objectId;
    }

    /**
     * @param int $objectId
     *
     * @return ProductImage
     */
    public function setObjectId($objectId)
    {
        $this->objectId = $objectId;

        return $this;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     *
     * @return ProductImage
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalImageUrl()
    {
        return $this->originalImageUrl;
    }

    /**
     * @param string $originalImageUrl
     *
     * @return ProductImage
     */
    public function setOriginalImageUrl($originalImageUrl)
    {
        $this->originalImageUrl = $originalImageUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * @param string $imagePath
     *
     * @return ProductImage
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isProcessingError()
    {
        return $this->processingError;
    }

    /**
     * @param boolean $processingError
     *
     * @return ProductImage
     */
    public function setProcessingError($processingError)
    {
        $this->processingError = (boolean) $processingError;

        return $this;
    }
}
