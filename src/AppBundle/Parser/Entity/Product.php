<?php

namespace AppBundle\Parser\Entity;

use AppBundle\Parser\Common\SEOFriendlyTrait;
use AppBundle\Parser\Common\DescriptiveTrait;

/**
 * Class Product
 *
 * @SuppressWarnings(PHPMD.TooManyFields)
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 *
 */
class Product extends ProductBase
{
    use SEOFriendlyTrait;
    use DescriptiveTrait;

    /** @var  float $bestPrice */
    protected $bestPrice;

    /** @var  float $bestPriceTax */
    protected $bestPriceTax;

    /** @var  float $bestTaxedPrice */
    protected $bestTaxedPrice;

    /** @var  boolean $translated */
    protected $translated;

    /** @var  string $locale */
    protected $locale;

    /** @var  string $title */
    protected $title;

    /** @var  string $url */
    protected $url;

    /** @var  int $position */
    protected $position;

    /** @var  boolean $visible */
    protected $visible;

    /** @var  int $defaultCategory */
    protected $defaultCategory;

    /** @var  int $taxRuleId */
    protected $taxRuleId;

    /** @var  int $brandId */
    protected $brandId;

    /** @var  boolean $showOriginalPrice */
    protected $showOriginalPrice;

    /** @var  int $productSaleElement */
    protected $productSaleElement;

    /** @var  int $productSaleElementCount */
    protected $productSaleElementCount;

    /**
     * @return float
     */
    public function getBestPrice()
    {
        return $this->bestPrice;
    }

    /**
     * @param float $bestPrice
     *
     * @return Product
     */
    public function setBestPrice($bestPrice)
    {
        $this->bestPrice = $bestPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getBestPriceTax()
    {
        return $this->bestPriceTax;
    }

    /**
     * @param float $bestPriceTax
     *
     * @return Product
     */
    public function setBestPriceTax($bestPriceTax)
    {
        $this->bestPriceTax = $bestPriceTax;

        return $this;
    }

    /**
     * @return float
     */
    public function getBestTaxedPrice()
    {
        return $this->bestTaxedPrice;
    }

    /**
     * @param float $bestTaxedPrice
     *
     * @return Product
     */
    public function setBestTaxedPrice($bestTaxedPrice)
    {
        $this->bestTaxedPrice = $bestTaxedPrice;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isTranslated()
    {
        return $this->translated;
    }

    /**
     * @param boolean $translated
     *
     * @return Product
     */
    public function setTranslated($translated)
    {
        $this->translated = (boolean) $translated;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     *
     * @return Product
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return Product
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return Product
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param int $position
     *
     * @return Product
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isVisible()
    {
        return $this->visible;
    }

    /**
     * @param boolean $visible
     *
     * @return Product
     */
    public function setVisible($visible)
    {
        $this->visible = (boolean) $visible;

        return $this;
    }

    /**
     * @return int
     */
    public function getDefaultCategory()
    {
        return $this->defaultCategory;
    }

    /**
     * @param int $defaultCategory
     *
     * @return Product
     */
    public function setDefaultCategory($defaultCategory)
    {
        $this->defaultCategory = $defaultCategory;

        return $this;
    }

    /**
     * @return int
     */
    public function getTaxRuleId()
    {
        return $this->taxRuleId;
    }

    /**
     * @param int $taxRuleId
     *
     * @return Product
     */
    public function setTaxRuleId($taxRuleId)
    {
        $this->taxRuleId = $taxRuleId;

        return $this;
    }

    /**
     * @return int
     */
    public function getBrandId()
    {
        return $this->brandId;
    }

    /**
     * @param int $brandId
     *
     * @return Product
     */
    public function setBrandId($brandId)
    {
        $this->brandId = $brandId;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isShowOriginalPrice()
    {
        return $this->showOriginalPrice;
    }

    /**
     * @param boolean $showOriginalPrice
     *
     * @return Product
     */
    public function setShowOriginalPrice($showOriginalPrice)
    {
        $this->showOriginalPrice = (boolean) $showOriginalPrice;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductSaleElement()
    {
        return $this->productSaleElement;
    }

    /**
     * @param int $productSaleElement
     *
     * @return Product
     */
    public function setProductSaleElement($productSaleElement)
    {
        $this->productSaleElement = $productSaleElement;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductSaleElementCount()
    {
        return $this->productSaleElementCount;
    }

    /**
     * @param int $productSaleElementCount
     *
     * @return Product
     */
    public function setProductSaleElementCount($productSaleElementCount)
    {
        $this->productSaleElementCount = $productSaleElementCount;

        return $this;
    }
}
