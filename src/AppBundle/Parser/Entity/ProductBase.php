<?php

namespace AppBundle\Parser\Entity;

class ProductBase
{
    /** @var  int $id */
    protected $id;

    /** @var  float $weight */
    protected $weight;

    /** @var  int $stock */
    protected $stock;

    /** @var  string $ref */
    protected $ref;

    /** @var  string $eanCode */
    protected $eanCode;

    /** @var  boolean $promo */
    protected $promo;

    /** @var  boolean $new */
    protected $new;

    /** @var  float $price */
    protected $price;

    /** @var  float $priceTax */
    protected $priceTax;

    /** @var  float $taxedPrice */
    protected $taxedPrice;

    /** @var  float $promoPrice */
    protected $promoPrice;

    /** @var  float $promoPriceTax */
    protected $promoPriceTax;

    /** @var  $taxedPromoPrice */
    protected $taxedPromoPrice;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     *
     * @return $this
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return int
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param int $stock
     *
     * @return $this
     */
    public function setStock($stock)
    {
        $this->stock = $stock;

        return $this;
    }

    /**
     * @return string
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param string $ref
     *
     * @return $this
     */
    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * @return string
     */
    public function getEanCode()
    {
        return $this->eanCode;
    }

    /**
     * @param string $eanCode
     *
     * @return $this
     */
    public function setEanCode($eanCode)
    {
        $this->eanCode = $eanCode;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isPromo()
    {
        return $this->promo;
    }

    /**
     * @param boolean $promo
     *
     * @return $this
     */
    public function setPromo($promo)
    {
        $this->promo = $promo;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * @param boolean $new
     *
     * @return $this
     */
    public function setNew($new)
    {
        $this->new = $new;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceTax()
    {
        return $this->priceTax;
    }

    /**
     * @param float $priceTax
     *
     * @return $this
     */
    public function setPriceTax($priceTax)
    {
        $this->priceTax = $priceTax;

        return $this;
    }

    /**
     * @return float
     */
    public function getTaxedPrice()
    {
        return $this->taxedPrice;
    }

    /**
     * @param float $taxedPrice
     *
     * @return $this
     */
    public function setTaxedPrice($taxedPrice)
    {
        $this->taxedPrice = $taxedPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getPromoPrice()
    {
        return $this->promoPrice;
    }

    /**
     * @param float $promoPrice
     *
     * @return $this
     */
    public function setPromoPrice($promoPrice)
    {
        $this->promoPrice = $promoPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getPromoPriceTax()
    {
        return $this->promoPriceTax;
    }

    /**
     * @param float $promoPriceTax
     *
     * @return $this
     */
    public function setPromoPriceTax($promoPriceTax)
    {
        $this->promoPriceTax = $promoPriceTax;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTaxedPromoPrice()
    {
        return $this->taxedPromoPrice;
    }

    /**
     * @param mixed $taxedPromoPrice
     *
     * @return $this
     */
    public function setTaxedPromoPrice($taxedPromoPrice)
    {
        $this->taxedPromoPrice = $taxedPromoPrice;

        return $this;
    }
}
