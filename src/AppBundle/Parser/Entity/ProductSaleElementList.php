<?php

namespace AppBundle\Parser\Entity;

use AppBundle\Parser\Common\ItemList;

class ProductSaleElementList extends ItemList
{
    const ENTITY_CLASS = ProductSaleElement::class;

    /**
     *
     */
    public function __construct()
    {
        parent::__construct(self::ENTITY_CLASS);
    }
}
