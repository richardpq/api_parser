<?php

namespace AppBundle\Parser\Entity;

use AppBundle\Parser\Common\ItemList;

class ProductImageList extends ItemList
{
    const ENTITY_CLASS = ProductImage::class;

    /**
     *
     */
    public function __construct()
    {
        parent::__construct(self::ENTITY_CLASS);
    }
}
