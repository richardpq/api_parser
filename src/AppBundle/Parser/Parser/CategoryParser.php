<?php

namespace AppBundle\Parser\Parser;

use AppBundle\Parser\Common\AbstractParser;
use AppBundle\Parser\Entity\Category;
use AppBundle\Parser\Entity\CategoryList;

class CategoryParser extends AbstractParser
{
    const ENTITY_CLASS = CategoryList::class;

    /** @var array $validKeys */
    protected $validKeys = array(
        'ID',
        'IS_TRANSLATED',
        'LOCALE',
        'TITLE',
        'CHAPO',
        'DESCRIPTION',
        'POSTSCRIPTUM',
        'PARENT',
        'ROOT',
        'URL',
        'META_TITLE',
        'META_DESCRIPTION',
        'META_KEYWORDS',
        'VISIBLE',
        'POSITION',
        "TEMPLATE",
        "VERSION",
        "VERSION_DATE",
        "VERSION_AUTHOR",
        "CREATE_DATE",
        "UPDATE_DATE",
        "LOOP_COUNT",
        "LOOP_TOTAL",
    );

    /**
     * CategoryParser constructor.
     */
    public function __construct()
    {
        $className = self::ENTITY_CLASS;
        parent::__construct(new $className());
    }

    /**
     * @inheritdoc
     *
     * @param array $record
     */
    protected function parseIndividualRecordToObject(array $record)
    {
        $category = new Category();

        $category
            ->setId($record['ID'])
            ->setTranslated($record['IS_TRANSLATED'])
            ->setLocale($record['LOCALE'])
            ->setTitle($record['TITLE'])
            ->setChapo($record['CHAPO'])
            ->setDescription($record['DESCRIPTION'])
            ->setPostscriptum($record['POSTSCRIPTUM'])
            ->setParent($record['PARENT'])
            ->setRoot($record['ROOT'])
            ->setUrl($record['URL'])
            ->setMetaTitle('META_TITLE')
            ->setMetaDescription('META_DESCRIPTION')
            ->setMetaKeywords('META_KEYWORDS')
            ->setVisible($record['VISIBLE'])
            ->setPosition($record['POSITION'])
        ;

        return $category;
    }
}
