<?php

namespace AppBundle\Parser\Parser;

use AppBundle\Parser\Common\AbstractParser;
use AppBundle\Parser\Entity\ProductImage;
use AppBundle\Parser\Entity\ProductImageList;

class ProductImageParser extends AbstractParser
{
    const ENTITY_CLASS = ProductImageList::class;

    /** @var array $validKeys */
    protected $validKeys = [
        "ID",
        "LOCALE",
        "ORIGINAL_IMAGE_PATH",
        "TITLE",
        "CHAPO",
        "DESCRIPTION",
        "POSTSCRIPTUM",
        "VISIBLE",
        "POSITION",
        "OBJECT_TYPE",
        "OBJECT_ID",
        "IMAGE_URL",
        "ORIGINAL_IMAGE_URL",
        "IMAGE_PATH",
        "PROCESSING_ERROR",
        "CREATE_DATE",
        "UPDATE_DATE",
        "LOOP_COUNT",
        "LOOP_TOTAL"
    ];

    /**
     *
     */
    public function __construct()
    {
        $className = self::ENTITY_CLASS;
        parent::__construct(new $className());
    }

    /**
     * @param array $record
     *
     * @return ProductImage
     */
    protected function parseIndividualRecordToObject(array $record)
    {
        $productImage = new ProductImage();
        $productImage
            ->setChapo($record['CHAPO'])
            ->setDescription($record['DESCRIPTION'])
            ->setPostscriptum($record['POSTSCRIPTUM'])
            ->setId($record['ID'])
            ->setLocale($record['LOCALE'])
            ->setOriginalImagePath($record['ORIGINAL_IMAGE_PATH'])
            ->setOriginalImageUrl($record['ORIGINAL_IMAGE_URL'])
            ->setImageUrl($record['IMAGE_URL'])
            ->setImagePath($record['IMAGE_PATH'])
            ->setTitle($record['TITLE'])
            ->setVisible($record['VISIBLE'])
            ->setPosition($record['POSITION'])
            ->setObjectId($record['OBJECT_ID'])
            ->setObjectType($record['OBJECT_TYPE'])
            ->setProcessingError($record['PROCESSING_ERROR'])
        ;

        return $productImage;
    }
}
