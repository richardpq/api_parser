<?php

namespace AppBundle\Parser\Parser;

use AppBundle\Parser\Common\AbstractParser;
use AppBundle\Parser\Entity\ProductSaleElement;
use AppBundle\Parser\Entity\ProductSaleElementList;

class ProductSaleElementParser extends AbstractParser
{
    const ENTITY_CLASS = ProductSaleElementList::class;

    /** @var array $validKeys */
    protected $validKeys = array(
        "ID",
        "QUANTITY",
        "IS_PROMO",
        "IS_NEW",
        "IS_DEFAULT",
        "WEIGHT",
        "REF",
        "EAN_CODE",
        "PRODUCT_ID",
        "PRICE",
        "PRICE_TAX",
        "TAXED_PRICE",
        "PROMO_PRICE",
        "PROMO_PRICE_TAX",
        "TAXED_PROMO_PRICE",
        "CREATE_DATE",
        "UPDATE_DATE",
        "LOOP_COUNT",
        "LOOP_TOTAL",
    );

    /**
     *
     */
    public function __construct()
    {
        $className = self::ENTITY_CLASS;
        parent::__construct(new $className());
    }

    /**
     * @param array $record
     *
     * @return ProductSaleElement
     */
    protected function parseIndividualRecordToObject(array $record)
    {
        $productSaleElement = new ProductSaleElement();

        $productSaleElement
            ->setId($record['ID'])
            ->setStock($record['QUANTITY'])
            ->setPromo($record['IS_PROMO'])
            ->setNew($record['IS_NEW'])
            ->setDefault($record['IS_DEFAULT'])
            ->setWeight($record['WEIGHT'])
            ->setRef($record['REF'])
            ->setEanCode($record['EAN_CODE'])
            ->setProductId($record['PRODUCT_ID'])
            ->setPrice($record['PRICE'])
            ->setPriceTax($record['PRICE_TAX'])
            ->setTaxedPrice($record['TAXED_PRICE'])
            ->setPromoPrice($record['PROMO_PRICE'])
            ->setPromoPriceTax($record['PROMO_PRICE_TAX'])
            ->setTaxedPromoPrice($record['TAXED_PROMO_PRICE'])
        ;

        return $productSaleElement;
    }
}
