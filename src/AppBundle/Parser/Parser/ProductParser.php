<?php

namespace AppBundle\Parser\Parser;

use AppBundle\Parser\Common\AbstractParser;
use AppBundle\Parser\Entity\Product;
use AppBundle\Parser\Entity\ProductList;

class ProductParser extends AbstractParser
{
    const ENTITY_CLASS = ProductList::class;

    /** @var array $validKeys */
    protected $validKeys = [
        "WEIGHT",
        "QUANTITY",
        "EAN_CODE",
        "BEST_PRICE",
        "BEST_PRICE_TAX",
        "BEST_TAXED_PRICE",
        "PRICE",
        "PRICE_TAX",
        "TAXED_PRICE",
        "PROMO_PRICE",
        "PROMO_PRICE_TAX",
        "TAXED_PROMO_PRICE",
        "IS_PROMO",
        "IS_NEW",
        "PRODUCT_SALE_ELEMENT",
        "PSE_COUNT",
        "ID",
        "REF",
        "IS_TRANSLATED",
        "LOCALE",
        "TITLE",
        "CHAPO",
        "DESCRIPTION",
        "POSTSCRIPTUM",
        "URL",
        "META_TITLE",
        "META_DESCRIPTION",
        "META_KEYWORDS",
        "POSITION",
        "VIRTUAL",
        "VISIBLE",
        "TEMPLATE",
        "DEFAULT_CATEGORY",
        "TAX_RULE_ID",
        "BRAND_ID",
        "SHOW_ORIGINAL_PRICE",
        "VERSION",
        "VERSION_DATE",
        "VERSION_AUTHOR",
        "CREATE_DATE",
        "UPDATE_DATE",
        "LOOP_COUNT",
        "LOOP_TOTAL"
    ];

    /**
     *
     */
    public function __construct()
    {
        $className = self::ENTITY_CLASS;
        parent::__construct(new $className());
    }

    /**
     * @inheritdoc
     */
    protected function parseIndividualRecordToObject(array $record)
    {
        $product = new Product();

        $product
            ->setWeight($record['WEIGHT'])
            ->setStock($record['QUANTITY'])
            ->setEanCode($record['EAN_CODE'])
            ->setPrice($record['PRICE'])
            ->setPriceTax($record['PRICE_TAX'])
            ->setBestPrice($record['BEST_PRICE'])
            ->setBestPriceTax($record['BEST_PRICE_TAX'])
            ->setBestTaxedPrice($record['BEST_TAXED_PRICE'])
            ->setTaxedPrice($record['TAXED_PRICE'])
            ->setPromoPrice($record['PROMO_PRICE'])
            ->setPromoPriceTax($record['PROMO_PRICE_TAX'])
            ->setTaxedPromoPrice($record['TAXED_PROMO_PRICE'])
            ->setPromo($record['IS_PROMO'])
            ->setRef($record['REF'])
            ->setTranslated($record['IS_TRANSLATED'])
            ->setLocale($record['LOCALE'])
            ->setTitle($record['TITLE'])
            ->setUrl($record['URL'])
            ->setPosition($record['POSITION'])
            ->setVisible($record['VISIBLE'])
            ->setDefaultCategory($record['DEFAULT_CATEGORY'])
            ->setTaxRuleId($record['TAX_RULE_ID'])
            ->setBrandId($record['BRAND_ID'])
            ->setShowOriginalPrice($record['SHOW_ORIGINAL_PRICE'])
            ->setMetaTitle($record['META_TITLE'])
            ->setMetaDescription($record['META_DESCRIPTION'])
            ->setMetaKeywords($record['META_KEYWORDS'])
            ->setChapo($record['CHAPO'])
            ->setDescription($record['DESCRIPTION'])
            ->setPostscriptum($record['POSTSCRIPTUM'])
            ->setNew($record['IS_NEW'])
            ->setProductSaleElement($record['PRODUCT_SALE_ELEMENT'])
            ->setProductSaleElementCount($record['PSE_COUNT'])
        ;

        return $product;

    }
}
