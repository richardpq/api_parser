<?php

namespace Tests\AppBundle\Parser\Entity;

use AppBundle\Parser\Entity\Category;

class CategoryTest extends \PHPUnit_Framework_TestCase
{
    public function testIsRoot()
    {
        $categoryRoot = new Category();
        $categoryRoot->setParent(0);

        $categoryNotRoot = new Category();
        $categoryNotRoot->setParent(5);

        $this->assertTrue($categoryRoot->isRoot());
        $this->assertFalse($categoryNotRoot->isRoot());
    }
}
