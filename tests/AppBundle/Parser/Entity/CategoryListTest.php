<?php

namespace Tests\AppBundle\Parser\Entity;

use AppBundle\Parser\Entity\CategoryList;
use AppBundle\Parser\Entity\Category;

class CategoryListTest extends \PHPUnit_Framework_TestCase
{
    public function testEntityClass()
    {
        $this->assertTrue(CategoryList::ENTITY_CLASS == Category::class);
    }
}
