<?php

namespace Tests\AppBundle\Parser\Common;

use AppBundle\Parser\Common\SEOFriendlyTrait;

class SEOFriendlyTraitTest extends \PHPUnit_Framework_TestCase
{
    public function testAccessorsMethods()
    {
        /** @var SEOFriendlyTrait $mock */
        $mock = $this->getMockForTrait('AppBundle\Parser\Common\SEOFriendlyTrait');

        $mock
            ->setMetaDescription('Meta Description')
            ->setMetaKeywords('Meta Keywords')
            ->setMetaTitle('Meta Title')
        ;

        $this->assertEquals('Meta Description', $mock->getMetaDescription());
        $this->assertEquals('Meta Keywords', $mock->getMetaKeywords());
        $this->assertEquals('Meta Title', $mock->getMetaTitle());
    }
}
