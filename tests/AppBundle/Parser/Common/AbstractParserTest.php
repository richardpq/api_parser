<?php

namespace Tests\AppBundle\Parser\Common;

use AppBundle\Parser\Common\AbstractParser;

class AbstractParserTest extends \PHPUnit_Framework_TestCase
{
    public function testConcreteMethods()
    {
        $stubItemList = $this->getMockBuilder('AppBundle\Parser\Common\ItemList')
            ->setConstructorArgs(array('stdClass'))
            ->setMethods(array('add'))
            ->getMock()
        ;

        $stubItemList
            ->expects($this->any())
            ->method('add')
            ->will($this->returnValue(true))
        ;

        /** @var AbstractParser|\PHPUnit_Framework_MockObject_MockObject $mock */
        $mock = $this->getMockBuilder('AppBundle\Parser\Common\AbstractParser')
            ->setMethods(['hasValidKeys'])
            ->setConstructorArgs(array($stubItemList))
            ->getMockForAbstractClass()
        ;

        $mock
            ->expects($this->any())
            ->method('hasValidKeys')
            ->will($this->returnValue(true))
        ;

        $this->assertInstanceOf('AppBundle\Parser\Common\ItemList', $mock->getObjectListFromRecords(array(
            [
                'KEY_ONE' => 'ONE',
                'KEY_TWO' => 'TWO',
            ],
            [
                'KEY_ONE' => 'ONE DIFFERENT',
                'KEY_TWO' => 'TWO DIFFERENT',
            ]

        )));
    }
}
