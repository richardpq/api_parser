<?php

namespace Tests\AppBundle\Parser\Common;

use AppBundle\Parser\Common\DescriptiveTrait;

class DescriptiveTraitTest extends \PHPUnit_Framework_TestCase
{
    public function testAccessorsMethods()
    {
        /** @var DescriptiveTrait $mock */
        $mock = $this->getMockForTrait('AppBundle\Parser\Common\DescriptiveTrait');

        $mock
            ->setChapo('Chapo')
            ->setDescription('Description')
            ->setPostscriptum('Postscriptum')
        ;


        $this->assertEquals('Chapo', $mock->getChapo());
        $this->assertEquals('Description', $mock->getDescription());
        $this->assertEquals('Postscriptum', $mock->getPostscriptum());
    }
}
