<?php

namespace Tests\AppBundle\Parser\Common;

use AppBundle\Parser\Common\ItemList;

class ItemListTest extends \PHPUnit_Framework_TestCase
{
    /** @var  ItemList $itemList */
    protected $itemList;

    public function setUp()
    {
        $this->itemList = new ItemList('stdClass');
    }

    public function testAdd()
    {
        $add = $this->itemList->add(new \stdClass());

        $this->assertTrue($add);
    }

    /**
     * @expectedException \Exception
     * @expectedExceptionMessage Item could not be added
     */
    public function testExceptionWhenAddIncorrectType()
    {
        $this->itemList->add(new ItemList('stdClass'));
    }

    public function testRemove()
    {
        $itemOne = new \stdClass();
        $itemTwo = new \stdClass();
        $itemTwo->name = "Test";
        $itemThree = new ItemList('stdClass');

        $this->itemList->add($itemOne);

        $this->assertTrue($this->itemList->remove($itemTwo) === null);
        $this->assertTrue($this->itemList->remove($itemThree) === null);
        $this->assertInstanceOf('stdClass', $this->itemList->remove($itemOne));
    }

    public function testCountable()
    {
        $itemOne = new \stdClass();
        $itemTwo = new \stdClass();
        $itemTwo->name = "Test";
        $itemThree = new \stdClass();
        $itemThree->name = "Test 3";

        $this->itemList->add($itemOne);
        $this->itemList->add($itemTwo);
        $this->itemList->add($itemThree);

        $this->assertCount(3, $this->itemList);
        $this->itemList->remove($itemOne);
        $this->assertCount(2, $this->itemList);
    }
}
